export interface NavbarConfig {
    path: string[];
    description: string;
}

export const Navbar: NavbarConfig[] = [
    {
        path: ['plans'],
        description: 'Choose plan'
    },
    {
        path: ['info'],
        description: 'Information'
    },
    {
        path: ['billing'],
        description: 'Billing'
    }
];
