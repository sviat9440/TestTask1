import { Component } from '@angular/core';
import { Navbar, NavbarConfig } from './navbar-config';
import { Router } from '@angular/router';

@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: [ './navbar.component.scss' ]
})
export class NavbarComponent {
    readonly config: NavbarConfig[] = Navbar;

    constructor (private router: Router) { }

    active (path: string[]) {
        return this.router.url === '/form/' + path.join('/');
    }

    // navigate(path: string[]) {
    //     this.router.navigate([environment.prefix].concat(path));
    // }
}
