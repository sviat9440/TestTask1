import { Component, Input } from '@angular/core';
import { FieldItem } from '../../types/forms';

@Component({
    selector: 'app-fields',
    templateUrl: './fields.component.html',
    styleUrls: [ './fields.component.scss' ]
})
export class FieldsComponent {
    @Input() fields: FieldItem[];

    compare(field: FieldItem) {
        return (item: any) => {
            console.log(item);
            return item === field.value;
        };
    }
}
