import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { BillingComponent } from './billing/billing.component';
import { InfoComponent } from './info/info.component';
import { PlansComponent } from './plans/plans.component';
import { TabsComponent } from './tabs.component';
import { SharedModule } from '../shared/shared.module';
import { FormsModule } from '@angular/forms';
import { FieldsComponent } from './fields/fields.component';

@NgModule({
    exports: [
        TabsComponent
    ],
    imports: [
        CommonModule,
        RouterModule,
        SharedModule,
        FormsModule
    ],
    declarations: [
        TabsComponent,
        BillingComponent,
        InfoComponent,
        PlansComponent,
        FieldsComponent
    ]
})
export class TabsModule {
}
