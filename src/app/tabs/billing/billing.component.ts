import { Component } from '@angular/core';
import { TabItem } from '../../types/forms';
import { DataService } from '../../shared/data.service';

@Component({
    selector: 'app-billing',
    templateUrl: './billing.component.html',
    styleUrls: [ './billing.component.scss' ]
})
export class BillingComponent {
    tab: TabItem;
    infoTab: TabItem;

    constructor (public dataService: DataService) {
        this.tab = this.dataService.tabs.billing;
        this.infoTab = this.dataService.tabs.info;
    }

    next() {
        alert('все');
    }
}
