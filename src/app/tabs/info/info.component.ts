import { Component } from '@angular/core';
import { TabItem } from '../../types/forms';
import { DataService } from '../../shared/data.service';
import { Router } from '@angular/router';

@Component({
    selector: 'app-info',
    templateUrl: './info.component.html',
    styleUrls: [ './info.component.scss' ]
})
export class InfoComponent {
    tab: TabItem;

    constructor (public dataService: DataService, private router: Router) {
        this.tab = this.dataService.tabs.info;
    }

    next() {
        this.router.navigate(['form', 'billing']);
    }
}
