import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { DataService } from '../../shared/data.service';

@Injectable({
    providedIn: 'root'
})
export class InfoGuard implements CanActivate {
    constructor(private dataService: DataService, private router: Router) {}

    canActivate (next: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        const result = !!this.dataService.selectedPlan;
        if (!result) {
            this.router.navigate(['form', 'plans']);
        }
        return !!this.dataService.selectedPlan;
    }
}
