import { Component, OnInit } from '@angular/core';
import { TabItem } from '../../types/forms';
import { DataService } from '../../shared/data.service';
import { Router } from '@angular/router';

@Component({
    selector: 'app-plans',
    templateUrl: './plans.component.html',
    styleUrls: [ './plans.component.scss' ]
})
export class PlansComponent implements OnInit {
    tab: TabItem;

    constructor (public dataService: DataService, private router: Router) {
        this.tab = this.dataService.tabs.plans;
    }

    ngOnInit () {
    }

    next() {
        this.router.navigate(['form', 'info']);
    }

}
