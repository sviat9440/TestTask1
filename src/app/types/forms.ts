export interface PlansFormItem {
    title: string;
    price: number;
    currency: string;
    preview: string;
}

export interface FieldValidator {
    pattern: string;
}

export interface FieldSelectItem {
    title: string;
    id: number;
}

export interface FieldItem {
    id: string;
    title: string;
    type: string;
    validators: FieldValidator[];
    items: FieldSelectItem[];
    value: any;
}

export interface TabItem {
    description: string;
    fields: FieldItem[];
    plans: PlansFormItem[];
}
