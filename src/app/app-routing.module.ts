import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PlansComponent } from './tabs/plans/plans.component';
import { TabsComponent } from './tabs/tabs.component';
import { InfoComponent } from './tabs/info/info.component';
import { InfoGuard } from './tabs/info/info.guard';
import { BillingComponent } from './tabs/billing/billing.component';
import { BillingGuard } from './tabs/billing/billing.guard';

const routes: Routes = [
    {
        path: '', redirectTo: 'form/plans', pathMatch: 'full'
    },
    {
        path: 'form', component: TabsComponent, children: [
            {
                path: 'plans', component: PlansComponent
            },
            {
                path: 'info', component: InfoComponent, canActivate: [InfoGuard]
            },
            {
                path: 'billing', component: BillingComponent, canActivate: [BillingGuard]
            },
            {
                path: '**', redirectTo: 'plans'
            }
        ]
    },
    {
        path: '**', redirectTo: 'form/plans'
    }
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule {
}
