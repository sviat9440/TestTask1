import { Injectable } from '@angular/core';
import { FieldItem, PlansFormItem, TabItem } from '../types/forms';

declare var require: any;

@Injectable({
    providedIn: 'root'
})
export class DataService {
    selectedPlan: PlansFormItem;
    tabs: any = {};

    constructor () {
        for (const file of [ 'plans', 'info', 'billing' ]) {
            const config = require('../../assets/config/' + file + '.json');
            this.tabs[ file ] = {
                description: config.description,
                fields: config.fields,
                plans: config.plans
            } as TabItem;
        }
    }

    parsePattern (pattern: string): RegExp {
        // Разбиваем строку на выраженгие и флаги для создания обьекта RegExp
        const blocks = pattern.split('/');
        blocks.splice(0, 1); // Удаление первого пустого элемента массива
        const flags = blocks.splice(blocks.length - 1, 1).join('');
        pattern = blocks.join('/');
        return new RegExp(pattern, flags);
    }

    validForm (fields: FieldItem[]) {
        for (const field of fields) {
            if (!field.value) {
                // Если поле не заполнено, форма не валидна
                return false;
            }
            if (field.validators) {
                for (const validator of field.validators) {
                    const reg = this.parsePattern(validator.pattern);
                    if (!reg.test(field.value)) {
                        // Если поле не соответствует одному из валидаторов, форма не валидна
                        return false;
                    }
                }
            }
        }
        // Иначе, форма валидна
        return true;
    }

    getValueById (id: string, fields: FieldItem[]) {
        for (const field of fields) {
            if (field.id === id) {
                return field.value;
            }
        }
    }
}
