import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RippleDirective } from './ripple/ripple.directive';

@NgModule({
    exports: [
        RippleDirective
    ],
    imports: [
        CommonModule
    ],
    declarations: [
        RippleDirective
    ]
})
export class SharedModule {
}
