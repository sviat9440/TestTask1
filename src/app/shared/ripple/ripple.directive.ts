import { Directive, ElementRef, Renderer2 } from '@angular/core';

@Directive({
    selector: 'button, [appRipple]'
})
export class RippleDirective {

    getOffset (element: any) {
        let left = 0;
        let top = 0;
        while (element && !isNaN(element.offsetLeft) && !isNaN(element.offsetTop)) {
            left += element.offsetLeft - element.scrollLeft;
            top += element.offsetTop - element.scrollTop;
            element = element.offsetParent;
        }
        return { top, left };
    }

    getScale (parent: HTMLElement): string {
        return 'scale(' + Math.max(parent.offsetHeight, parent.offsetWidth) / 20 + ')';
    }

    addRipple ({ layerX, layerY }: any, parent: HTMLElement, renderer: Renderer2): HTMLElement {
        const ripple = renderer.createElement('div');
        ripple.className = 'ripple hide';
        ripple.style.top = layerY - 25 + 'px';
        ripple.style.left = layerX - 25 + 'px';
        parent.appendChild(ripple);
        setTimeout(() => {
            ripple.classList.remove('hide');
            ripple.style.transform = this.getScale(parent);
        }, 0);
        return ripple;
    }

    addRemoveHandler (events: Array<string>, obj: HTMLElement, parent: HTMLElement) {
        const listener = () => {
            events.forEach((event) => {
                parent.removeEventListener(event, listener);
            });
            setTimeout(() => {
                obj.classList.add('hide');
                setTimeout(() => {
                    parent.removeChild(obj);
                }, 700);
            }, 70);
        };
        events.forEach((event) => {
            parent.addEventListener(event, listener);
        });
    }

    constructor ({ nativeElement }: ElementRef, renderer: Renderer2) {
        if (window.ontouchstart === undefined) {
            nativeElement.addEventListener('mousedown', (e: MouseEvent) => {
                this.addRemoveHandler([ 'mouseup', 'mouseleave' ], this.addRipple(e, nativeElement, renderer), nativeElement);
            });
        } else {
            nativeElement.addEventListener('touchstart', (e: TouchEvent) => {
                const { left, top } = this.getOffset(nativeElement);
                const touch = e.touches[0];
                const { clientX, clientY } = touch;
                const [ layerX, layerY ] = [ clientX - left, clientY - top ];
                this.addRemoveHandler([ 'touchcancel', 'touchend' ], this.addRipple({
                    layerX,
                    layerY
                }, nativeElement, renderer), nativeElement);
            });
        }
    }

}
